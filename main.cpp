#include <iostream>
#include <time.h>
#include "ModelePredictif.h"
#include "Analyse.h"
#include "FonctionsExternes.h"
#include <vector>
using namespace std;

int main()
{
    FonctionsExternes fe;

    string id, mdp;
    string tmp = "";
	do {
		cout << "Veuillez saisir votre identifiant : " << endl;
		cin >> id;

		cout << "Veuillez saisir votre mot de passe : " << endl;
		cin >> mdp;
	} while (fe.connexion(id, mdp) == 0);
    
    Analyse a;
    ModelePredictif modPred;
	cout << "Chargement en cours..." << endl;
    a.setModelePredictif(modPred);
	clock_t t = clock();
    a.getModelePredictif().mesurerMaladie();
    a.getModelePredictif().setNomsAttributs();
	t = clock() - t;
	cout << "temps (en ms) : " << t << endl;

    int nb = 0;

    while(nb != 5) {
		cout << "********************Menu********************" << endl;
		cout << "1. Afficher les maladies prises en compte par le systeme" << endl;
		cout << "2. Voir les types des attributs d'empreinte" << endl;
		cout << "3. Analyser une ou des empreinte(s)" << endl;
		cout << "4. Calculer le taux de fiabilite du modele" << endl;
		cout << "5. Se deconnecter" << endl << endl;

        cout << "Veuillez saisir une option : " << endl;
        cin >> tmp;
        cout << endl;
        
        while(!isdigit(tmp[0]))
        {
            cout << "Cette option n'est pas valide. Veuillez ressaisir : " << endl;
            cin >> tmp;
        }

        nb = atoi(&tmp[0]);
        switch(nb)
        {
            case(1):
            {

                cout << "Les maladies prises en compte par le systeme sont : " << endl;
                a.getModelePredictif().afficherMaladie();
                cout << endl;
                cout << "Tapez '+' pour afficher les maladies et leurs details, tapez 'retour' pour revenir en arriere." << endl;
                cin >> tmp;
                cout << endl << endl;

                while (tmp.compare("+") != 0 && tmp.compare("retour") != 0) {
                    cout << "La commande saisie n'est pas valide. Veuillez ressaisir : " << endl;
                    cin >> tmp;
                } 
                
                if (tmp.compare("+")  == 0)
                {
                    cout << "Detail des maladies : " << endl;
                    a.getModelePredictif().afficherMaladieDetail();
                }
                else if (tmp.compare("retour") == 0)
                {
                    cout << endl;
                }    
				break;
            }
            case(2):
            {
                cout << "Affichage des attributs d'empreinte du systeme et leur type : " << endl;
                fe.lireLigneAttComplet();
                cout << endl;
                break;
            }
            case(3):
            {
				fe.viderFichier("sortie.txt");
				cout << "Souhaitez-vous afficher les resultats a l'ecran en plus de les ecrire dans le fichier \"sortie.txt\" ? Tapez 'O' pour les afficher, 'N' sinon. " << endl;
				cin >> tmp;
				while (tmp != "O" && tmp != "N")
				{
					cout << "La commande saisie n'est pas valide. Veuillez ressaisir :" << endl;
					cin >> tmp;
				}
				cout << endl << endl;
				string nFic;
				cout << "Veuillez renseigner le nom du fichier txt a analyser : ";
				cin >> nFic;
				clock_t t = clock();
				if (tmp == "O")
				{
					vector<vector<string>> vvs = fe.lireEmpreintes(nFic);
					cout << endl;
					a.calculerResultat(vvs, false);
				}
				else
				{
					vector<vector<string>> vvs = fe.lireEmpreintes(nFic);
					cout << endl;
					a.calculerResultat(vvs, true);
				}
				t = clock() - t;
				cout << "temps (en ms) : " << t << endl;
				cout << endl << endl;
                break;  
            }
            case(4):
            {
				clock_t t = clock();
				a.calculerFiabiliteModele();
				t = clock() - t;
				cout << "temps (en ms) : " << t << endl;
                break;
            }
            case(5):
            {
                cout << "Au revoir !" << endl;
                break;
            }
            default:
            {
                cout << "Cette option n'est pas valide." << endl;
                break;
            }
        }
    }
    return 0;
}