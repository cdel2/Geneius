#include <iostream>
#include <string.h>  
#include "Analyse.h"
#include "FonctionsExternes.h"
using namespace std;

vector<vector <pair<string, double>>> Analyse::trouverMaladies(vector<vector<string>> & empreintes)
{
	vector<vector <pair<string, double>>> maladies;

	for (unsigned int i=0; i<empreintes.size(); i++)
	{
		vector<string> ligneAttributs = empreintes[i];
		vector <pair<string, double>> maladiesProbables = comparerModele(ligneAttributs);
		maladies.push_back(maladiesProbables);
	}
	return maladies;
}



void Analyse::calculerResultat(vector<vector<string>> & empreintes, bool nePasAfficher)
{
	vector<vector <pair<string, double>>> vvs = trouverMaladies(empreintes);
	vector<vector <pair<string, double>>>::iterator it;
	string tmp;
	/*
	for (it = vvs.begin(); it != vvs.end(); it++)
	{
		if ((*it).size() != 0)
		{
			tmp = (*it)[0].first;
		}
		else
		{
			tmp = "";
		}
	}*/
	fe.viderFichier("sortie.txt");
	fe.ecrireFichierSortie("NoID;Maladie;Probabilite\n");
	for (unsigned int i = 0; i < vvs.size(); i++)
	{
		if(!nePasAfficher)
			cout << "empreinte " << i << " : " << endl;
		fe.ecrireFichierSortie(to_string(i) + ";");
		for (unsigned int j = 0; j < vvs[i].size(); j++)
		{
			fe.ecrireFichierSortie(vvs[i][j].first + ";" + to_string(vvs[i][j].second));
			if (j != vvs[i].size()-1)
			{
				fe.ecrireFichierSortie(";");
			}
			if (!nePasAfficher)
				cout << "maladie : " << vvs[i][j].first << "; probabilite : " << vvs[i][j].second << " ; " << endl;
		}
		if (i!= vvs.size()-1)
		{
			fe.ecrireFichierSortie("\n");
		}
		if (!nePasAfficher)
			cout << endl;
	}
	cout << endl << endl;
}



vector <pair<string, double>> Analyse::comparerModele(vector <string> & empreinte)
{
	map<string, Maladie>::iterator itMal;
	vector<string>::iterator itAtt;
	double sum; //nombre de match pour chaque ligne avec chaque maladie
	int cpt; //numero de l'attribut en cours de parcours
	vector <pair<string, double>> maladiesDetectees;
	for (itMal = mp.getTabMaladies().begin(); itMal != mp.getTabMaladies().end(); itMal++) //on parcourt toutes les maladies constitutives de notre modele
	{	//pour toutes les maladies
		sum = 0;
		cpt = 0;
		for (itAtt = empreinte.begin(); itAtt != empreinte.end(); itAtt++) //pour chaque attribut de la ligne 
		{	//pour tous les attributs de cette maladie
			if (isdigit((*itAtt)[0]) || (*itAtt)[0] == '-')
			{	//si l'attribut est numerique
				double vlr = stod(*itAtt);
				double moy = itMal->second.getTauxAttributs()[cpt].getMoyenne();
				double ec = itMal->second.getTauxAttributs()[cpt].getEcartType();
				sum += exp(-pow((vlr - moy) / ec, 2));	//courbe de gauss
			}
			else
			{
				map<string, double> mapFrequenceString = itMal->second.getTauxAttributs()[cpt].getFrequenceString();
				map<string, double>::iterator itFrequenceString = mapFrequenceString.find(*itAtt);
				if (itFrequenceString != mapFrequenceString.end())
				{
					sum += itFrequenceString->second;
				}
			}
			cpt++;
		}
		double pourcentageValidite = 0;
		if (empreinte.size() != 0)
		{
			pourcentageValidite = (double)sum / empreinte.size();
		}
		if (pourcentageValidite >= 0.5)
		{	//si le patient a plus de 50% de chance d'avoir la maladie, on la rajoute dans les maladies potentielles
			maladiesDetectees.push_back(make_pair(itMal->first, pourcentageValidite));
		}
	}
	maladiesDetectees = fe.sortMaladiesDetectees(maladiesDetectees);
	return maladiesDetectees;
}


double Analyse::calculerFiabiliteModele()
{
	cout << "Calcul en cours..." << endl;

	//realiser une analyse d'un fichier d'empreinte de reference
	//lire le fic Ref et le stocker dans un vector<string>, ne pas mettre le dernier attribut
	vector<vector<string>> ficRef = fe.lireFichierRef("HealthMeasurementsWithLabels.txt");
	vector<vector<string>> empreintes;
	vector<string>v;
	vector<string> maladies;
	for (unsigned int i = 0; i< ficRef.size(); i++)
	{	//recuperer les maladies d'un cote, les attributs des empreintes de l'autre dans vvs
		maladies.push_back(ficRef[i][0]);
		for (unsigned int j = 1; j < ficRef[i].size(); j++)
		{
			v.push_back(ficRef[i][j]);
		}
		empreintes.push_back(v);
		v.clear();
	}
	fe.viderFichier("sortie.txt");
	calculerResultat(empreintes, true);
	//lire le fichier de sortie
	vector<vector<string>> vvsSortie = fe.lireFichierRef("sortie.txt");
	int cmptMatch = 0;

	for (unsigned int i = 0; i < vvsSortie.size(); i++)
	{	//pour toutes les empreintes, comparer la maladie trouvee (dans le fichier de sortie)
		//a la maladie indiquee dans le fichier d'entree. Si c'est la meme, faire cpt++
		if (vvsSortie[i].size() == 1 && maladies[i].compare("")==0)
		{	//si pas de maladie
			cmptMatch++;
		}
		if (vvsSortie[i].size() > 1 && maladies[i].compare(vvsSortie[i][1])==0)
		{	//si la maladie trouvee est la bonne
			cmptMatch++;
		}
	}
	double fiabilite = (double)cmptMatch / vvsSortie.size();
	cout << "Taux de fiabilite du modele : " << (int)(100 * fiabilite) << '%' << endl;
	return fiabilite;
}

ModelePredictif & Analyse::getModelePredictif()
{
	return mp;
}


void Analyse::setModelePredictif(ModelePredictif m)
{
	mp = m;
}