#include <vector>
#include <iostream>
#include <string>
#include <math.h>
#include "Maladie.h"
#include "Info.h"
using namespace std;


Maladie::Maladie()
{
}

vector <Info> Maladie::MAJInfoMaladie(string & valAtt, unsigned int numAtt)
{
	if (isdigit(valAtt[0]) || valAtt[0] == '-') //on teste si le premier caractere de l'attribut est numerique => attribut numerique
	{
		double vanum = stod(valAtt); //conversion string => double
		if (tauxAttributs.size() <= numAtt) //si l'attribut n'existe pas deja
		{
			Info info;
			info.getValeursD().push_back(vanum);
			info.setIsNumeric(true);
			tauxAttributs.push_back(info);
		}else
		{
			tauxAttributs.at(numAtt).getValeursD().push_back(vanum);
		}
	}
	else
	{
		if (tauxAttributs.size() <= numAtt) 
		{
			Info info;
			info.getValeursS().push_back(valAtt);
			info.setIsNumeric(false);
			tauxAttributs.push_back(info);
		}
		else
		{
			tauxAttributs.at(numAtt).getValeursS().push_back(valAtt);
		}
	}
	return tauxAttributs;
}

void Maladie::calculMoyenne()
{
	vector<Info>::iterator itvinfo;
	vector<double>::iterator itvDouble;
	double moy;
	for(itvinfo = tauxAttributs.begin(); itvinfo != tauxAttributs.end(); itvinfo++)
	{
		moy = 0;
		if((*itvinfo).getIsNumeric())
		{
			for(itvDouble = ((*itvinfo).getValeursD()).begin(); itvDouble != ((*itvinfo).getValeursD()).end(); itvDouble++)
			{
				moy += *itvDouble;
			}
			moy /= itvinfo->getValeursD().size();
			(*itvinfo).setMoyenne(moy);
		}
	}
}

void Maladie::calculEcartType()
{
	vector<Info>::iterator itvinfo;
	vector<double>::iterator itvDouble;
	double ecartType;

	for(itvinfo = tauxAttributs.begin(); itvinfo != tauxAttributs.end(); itvinfo++)
	{
		ecartType = 0;
		if((*itvinfo).getIsNumeric())
		{
			for(itvDouble = itvinfo->getValeursD().begin(); itvDouble != itvinfo->getValeursD().end(); itvDouble++)
			{
				ecartType += pow(*itvDouble -itvinfo->getMoyenne(), 2);
			}
			
			(*itvinfo).setEcartType( sqrt(ecartType/ itvinfo->getValeursD().size()));
		}
	}
}


void Maladie::calculStringFrequent()
{
	vector<Info>::iterator itvinfo;
	vector<string>::iterator itvString;

	for (itvinfo = tauxAttributs.begin(); itvinfo != tauxAttributs.end(); itvinfo++)
	{//parcours de tous les attributs
		if (!(*itvinfo).getIsNumeric())
		{	//si c'est un string
			map <string, double> cmptString;
			for (itvString = itvinfo->getValeursS().begin(); itvString != itvinfo->getValeursS().end(); itvString++)
			{	//parcours de toutes les valeurs enregistrees pour l'attribut
				pair<string, double> stringAAjouter(*itvString, 1);
				pair<map <string, double>::iterator, bool> ajoutFait = cmptString.insert(stringAAjouter);
				if (!ajoutFait.second)
				{	//si le string avait deja ete trouve, faire le compteur ++
					(*ajoutFait.first).second++;
				}
			}
			double max = 1;
			for (map <string, double>::iterator it = cmptString.begin(); it != cmptString.end(); it++)
			{	//normaliser les valeurs : frequence/frequenceMax
				if ((*it).second>max)
				{
					max = (*it).second;
				}
			}
			for (map <string, double>::iterator it = cmptString.begin(); it != cmptString.end(); it++)
			{	//normaliser les valeurs : frequence/frequenceMax
				it->second = it->second / max;	//max > 0 tout le temps
			}
			itvinfo->setFrequenceString(cmptString);
		}
	}
}

void Maladie::calculStatistiques()
{
	calculMoyenne();
	calculEcartType();
	calculStringFrequent();
}


vector<Info> & Maladie::getTauxAttributs()
{
	return tauxAttributs;
}