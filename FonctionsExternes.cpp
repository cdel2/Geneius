#include "FonctionsExternes.h"


int FonctionsExternes::connexion(const string username, const string password) {
	in.open("user.txt", ios::in);
	if (in)  // si l'ouverture a russi
	{
		// instructions
		string ligne;
		string us, mdp;
		while (getline(in, ligne))  // tant que l'on peut mettre la ligne dans "contenu"
		{
			size_t pos = ligne.find(" ");      // position of "live" in str

			us = ligne.substr(0, pos);
			mdp = ligne.substr(pos + 1);

			if (username.compare(us) == 0 && password.compare(mdp) == 0)
			{
				cout << endl;
				cout << "Content de vous revoir,  " + username + "." << endl << endl;
				in.close();
				return 1;
			}
		}
		//Si aucune ligne ne correspond aux identifiants entres, executer les trois lignes suivantes
		cerr << "Erreur : identifiant et/ou mot de passe invalides." << endl << endl;
		in.close();
		return 0;
	}
	else { // sinon
		cerr << "Impossible d'acceder a la base de donnees." << endl;
	}
	in.close();
	return 0;
}


void FonctionsExternes::ecrireLog(string s) 
{
	out.open("log.txt", ios::app);
	if (out) {
		out << s << endl;
		out.close();
	}
}

void FonctionsExternes::ecrireFichierSortie(string s) 
{
	out.open("sortie.txt", ios::app);
	if (out) {
		out << s;
		out.close();
	}
}

void FonctionsExternes::viderFichier(string nFic)
{
	ofstream out;
	out.open(nFic, ios::trunc);
	out.close();
}


map <string, string> FonctionsExternes::lireLigneAttComplet(string nFic)
{
	file.open(nFic, ios::in);//ouvrir fichier
	string line;
	map <string, string> mss;
	cout << "attribut : type "<< endl;
	if (file) {
		getline(file, line);
		getline(file, line);

		while (!file.eof())
		{
			getline(file,line);
			size_t pos = line.find(";");
			string attribut = line.substr(0, pos);
			string type = line.substr(pos + 1, line.length() - (pos + 1));

			if (attribut.compare("") != 0 && type.compare("") != 0) {
				cout << attribut << " : " << type << endl;
				pair <string, string> p = make_pair(attribut, type);
				mss.insert(p);
			}
		}
	}

	file.close();
	return mss;
}

vector <string> FonctionsExternes::lireLigneAtt(string nFic)
{
	file.open(nFic, ios::in);//ouvrir fichier
	string line;
	vector <string> vs;
	if (file) {
		getline(file, line);
		getline(file, line);

		while (!file.eof())
		{
			getline(file,line);
			size_t pos = line.find(";");
			string attribut = line.substr(0, pos);
			vs.push_back(attribut);
		}
	}
	file.close();
	return vs;
}


vector<vector<string>> FonctionsExternes::lireFichierRef(string nFic) {
	//stocker les empreintes des attributs dans un vecteur de type string
	vector<string> v;
	vector<vector<string>> empreintes;
	empreintes.reserve(100000);
	file.open(nFic);
	if (file.is_open()) {
		string line;
		getline(file, line);
		while (!file.eof()) {
			v.reserve(10);
			getline(file, line);
			size_t posID = line.find_first_of(";");
			size_t posMaladie = line.find_last_of(";");
			string maladie = line.substr(posMaladie + 1);
			string desAttributs = line.substr(posID + 1, posMaladie - posID);
			if (maladie.compare("") != 0) {
				v.push_back(maladie);
				while (desAttributs.compare("") != 0) {
					size_t posNextVirgule = desAttributs.find_first_of(";");
					string unAttribut = desAttributs.substr(0, posNextVirgule);
					v.push_back(unAttribut);
					desAttributs = desAttributs.substr(posNextVirgule + 1);
				}
			}
			else {
				v.push_back("");
			}
			empreintes.push_back(v);
			v.clear();
		}
		file.close();
	}
	return empreintes;
}

vector<vector<string>> FonctionsExternes::lireEmpreintes(string nFic) {
	//stocker les empreinte des attributs dans un vecteur de type string
	vector<string> v;
	vector<vector<string>> empreintes;
	empreintes.reserve(100000);
	file.open(nFic);
	if (file.is_open()) {
		string line;
		getline(file, line);
		while (!file.eof()) {
			v.reserve(10);
			getline(file, line);
			size_t posID = line.find_first_of(";");
			string desAttributs = line.substr(posID + 1);
			size_t posNextVirgule=desAttributs.find_first_of(";");
			while (desAttributs.compare("") != 0 && posNextVirgule!=string::npos) {
				string unAttribut = desAttributs.substr(0, posNextVirgule);
				v.push_back(unAttribut);
				desAttributs = desAttributs.substr(posNextVirgule + 1);
				posNextVirgule = desAttributs.find_first_of(";");
			}
			empreintes.push_back(v);
			v.clear();
		}
		file.close();
	}
	return empreintes;
}



vector <pair<string, double>> FonctionsExternes::sortMaladiesDetectees(vector <pair<string, double>>& maladiesDetectees)
{	//trier les maladies potentielles par probabilites decroissantes
	bool modificationFaite = true;
	if ((maladiesDetectees).size()>1)
	{
		while (modificationFaite)
		{
			modificationFaite = false;
			for (unsigned int i = 0; i<(maladiesDetectees).size() - 1; i++)
			{
				if ((maladiesDetectees)[i].second < (maladiesDetectees)[i + 1].second)
				{
					pair<string, double> tmp = (maladiesDetectees)[i];
					(maladiesDetectees)[i] = (maladiesDetectees)[i + 1];
					(maladiesDetectees)[i + 1] = tmp;
					modificationFaite = true;
				}
			}
		}
	}

	return maladiesDetectees;
}

FonctionsExternes::FonctionsExternes() {}

FonctionsExternes::~FonctionsExternes() {}
