/*************************************************************************
FonctionsExternes  -  description
-------------------
d閎ut                : ${date}
copyright            : (C) ${year} par ${user}
*************************************************************************/

//---------- Interface de la classe <FonctionsExternes> (fichier ${file_name}) ------
#ifndef FONCTIONSEXTERNES_H
#define FONCTIONSEXTERNES_H

//--------------------------------------------------- Interfaces utilisés
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <map>
#include <cstdlib>
#include <vector>
#include <algorithm>


using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <FonctionsExternes>
//
//
//------------------------------------------------------------------------

class FonctionsExternes
{
	//----------------------------------------------------------------- PUBLIC

public:
	//----------------------------------------------------- Méthodes publiques
	int connexion(string username, string password);
	// Mode d'emploi :
	//
	// Contrat :
	//
	size_t stringCount(const string& referenceString, const string& subString);
	void ecrireLog(string s);
	void ecrireModele(string s);
	void ecrireFichierSortie(string s);
	void viderFichier(string nFic);


	map <string, string> lireLigneAttComplet(string nFic = "metaDonnees.txt");
	vector <string> lireLigneAtt(string nFic = "metaDonnees.txt");


	vector<vector<string>> lireFichierRef(string nFic);
	vector<vector<string>> lireEmpreintes(string nFic);	//AAnalyser

	//tri par valeurs décroissantes de double (fréquence)
	vector <pair<string, double>> sortMaladiesDetectees(vector <pair<string, double>>& maladiesDetectees);

	//------------------------------------------------- Surcharge d'opéateurs


	//-------------------------------------------- Constructeurs - destructeur

	FonctionsExternes();
	// Mode d'emploi :
	//
	// Contrat :
	//

	virtual ~FonctionsExternes();
	// Mode d'emploi :
	//
	// Contrat :
	//

	//------------------------------------------------------------------ PRIVE

protected:
	//----------------------------------------------------- Méthodes protégées

private:
	//------------------------------------------------------- Méthodes privées

protected:
	//----------------------------------------------------- Attributs protégés
	ofstream out;
	ifstream in;
	fstream file;
private:
	//------------------------------------------------------- Attributs privés

	//---------------------------------------------------------- Classes amies

	//-------------------------------------------------------- Classes privées

	//----------------------------------------------------------- Types privés

};

//----------------------------------------- Types dépendants de <FonctionsExternes>

#endif // FONCTIONSEXTERNES_H
