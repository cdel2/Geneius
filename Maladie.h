#ifndef MALADIE_H
#define MALADIE_H

#include <vector>
#include <string>
#include "Info.h"
using namespace std;

class Maladie
{
    public:
        Maladie();
        Maladie(string nomMaladie): nom(nomMaladie)
        {}

		vector <Info> MAJInfoMaladie(string& valAtt, unsigned int numAtt);
        // Mode d'emploi :
        // mise à jour des infos (écart-type/moyenne des attributs) d'une maladie
        // à partir du numéro de l'attribut et sa valeur pour une certaine ligne

        void calculMoyenne();
        void calculEcartType();
		void calculStringFrequent();

		void calculStatistiques();

		vector<Info> & getTauxAttributs();

	private :
		string nom;
		vector<Info> tauxAttributs; //attributs rangés toujours dans le meme ordre

};

#endif