#ifndef INFO_H
#define INFO_H

#include <map>
#include <vector>
#include <string>

using namespace std;

class Info
{
public:
	bool getIsNumeric();
	void setIsNumeric(bool isNum);

	vector<double> & getValeursD();
	double getMoyenne();
	void setMoyenne(double moy);
	double getEcartType();
	void setEcartType(double ec);

	vector <string> & getValeursS();
	map<string, double> & getFrequenceString();
	void setFrequenceString(map<string, double> fs);

private:
	bool isNumeric;
	
	vector <double> valeursD;
	double moyenne;
	double ecartType;


	vector <string> valeursS;
	map<string, double> frequenceString;
};

#endif