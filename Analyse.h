#ifndef ANALYSE_H
#define ANALYSE_H
#include "ModelePredictif.h"
#include "Maladie.h"
#include "Info.h"
#include "FonctionsExternes.h"

class Analyse
{
    public:
		vector<vector <pair<string, double>>> trouverMaladies(vector<vector<string>> & vvs);

		//permet de comparer une ligne d'attributs (empreinte) avec le modèle calculé pour trouver
		//les maladies probables pour cette empreinte, avec leur probabilité
		vector <pair<string, double>> comparerModele(vector <string> & empreinte);
		

		void calculerResultat(vector<vector<string>> & empreintes, bool nePasAfficher=false);

		//permet de calculer le taux de fiabilité du modèle, entre 0 et 1
		double calculerFiabiliteModele();
		
		//Modele Predictif utilisé pour les analyses d'empreintes
		ModelePredictif & getModelePredictif();
		void setModelePredictif(ModelePredictif m);

	private :
        ModelePredictif mp;
		FonctionsExternes fe;
};
#endif