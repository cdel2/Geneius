#ifndef MODELEPREDICTIF_H
#define MODELEPREDICTIF_H

#include <map>
#include <string>
#include "Info.h"
#include "Maladie.h"
using namespace std;

class ModelePredictif
{
public:
	

	void mesurerMaladie();
	// Mode d'emploi : 
	// Remplie l'attribut tabMaladies

	void setNomsAttributs();
	void afficherNomsAttributs();
	void afficherMaladie();
	void afficherMaladieDetail();
	map<string, Maladie> & getTabMaladies();

private:
	map<string, Maladie> tabMaladies;
	vector <string> tabNomsAttributs;

};

#endif