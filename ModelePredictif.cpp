#include <string>
#include <map>
#include <iterator>
#include <vector>
#include <iostream>
#include "Maladie.h"
#include "ModelePredictif.h"
#include "FonctionsExternes.h"

using namespace std;


void ModelePredictif::mesurerMaladie()	
{
	FonctionsExternes fe;
	vector<vector<string>> empreintesRef = fe.lireFichierRef("HealthMeasurementsWithLabels.txt");
	
	for (unsigned int itEmpreinte = 0; itEmpreinte < empreintesRef.size(); itEmpreinte++)
	{
		vector<string> ligne = empreintesRef[itEmpreinte]; //on recupere les valeurs d'une ligne
		string nomMaladie = ligne[0];
		if (strcmp(nomMaladie.c_str(), "") != 0)
		{
			//on rajoute chaque maladie differente lue dans notre tableau de maladies
			Maladie maladie(nomMaladie);
			pair<string, Maladie> maladieAAjouter(nomMaladie, maladie);
			map<string, Maladie>::iterator resultatInsertion = tabMaladies.insert(maladieAAjouter).first;

			//mise a jour des infos pour chaque attribut de chaque maladie
			for (unsigned int itAtt = 1; itAtt < ligne.size(); itAtt++)
			{
				(resultatInsertion->second).MAJInfoMaladie(ligne[itAtt], itAtt - 1);
			}
		}
	}
	for (map<string,Maladie>::iterator itMal = tabMaladies.begin(); itMal != tabMaladies.end(); itMal++)
	{//pour tous les attributs de toutes les maladies, calculer les statistiques
		itMal->second.calculStatistiques();
	}
}


void ModelePredictif::afficherMaladie()
{
	for(map<string, Maladie>::iterator itMal = tabMaladies.begin(); itMal != tabMaladies.end(); itMal++)
	{
		cout << itMal->first << endl;
	}
	cout << endl;
}

void ModelePredictif::afficherMaladieDetail()
{
	int j;
	FonctionsExternes fe;
	for (map<string, Maladie>::iterator itMal = tabMaladies.begin(); itMal != tabMaladies.end(); itMal++)
	{//parcourir les maladies
		cout << endl << itMal->first << endl;
		Maladie maladie = itMal->second;
		vector<Info> donneesMaladie = maladie.getTauxAttributs();
		j = 0;
		for (vector<Info>::iterator itAtt = donneesMaladie.begin(); itAtt != donneesMaladie.end(); itAtt++)
		{//parcourir les attributs
			if (itAtt->getIsNumeric())
			{	//pour un attribut numerique, on affiche sa moyenne et son ecart type
				cout << "attribut : " << tabNomsAttributs[j] << "; moyenne : " << itAtt->getMoyenne() << "; ecart type : " << itAtt->getEcartType() << endl;
			}
			else
			{	//pour un attribut string, on affiche le string le plus frequent
				vector<pair<string, double>> frequenceS;
				for (auto itFreqStr = itAtt->getFrequenceString().begin(); itFreqStr != itAtt->getFrequenceString().end(); itFreqStr++)
				{
					frequenceS.push_back(make_pair(itFreqStr->first, itFreqStr->second));
				}
				frequenceS = fe.sortMaladiesDetectees(frequenceS);
				cout << "attribut : " << tabNomsAttributs[j] << "; chaine la plus frequente : " << frequenceS[0].first << endl;
			}

			++j;
		}
	}
	cout << endl;
}

void ModelePredictif::setNomsAttributs()
{
	FonctionsExternes fe;
	tabNomsAttributs = fe.lireLigneAtt();
}

void ModelePredictif::afficherNomsAttributs()
{
	cout << "Attributs pris en compte par le systeme : " << endl;
	for(vector <string>::iterator it = tabNomsAttributs.begin(); it != tabNomsAttributs.end(); it++)
	{
		cout << *it << endl;
	}
}

map<string, Maladie> & ModelePredictif::getTabMaladies()
{
	return tabMaladies;
}
