#include <map>
#include <vector>
#include <string>
#include "Info.h"
using namespace std;

bool Info::getIsNumeric()
{
	return isNumeric;
}

void Info::setIsNumeric(bool isNum)
{
	isNumeric = isNum;
}

vector<double> & Info::getValeursD()
{
	return valeursD;
}

double Info::getMoyenne()
{
	return moyenne;
}

void Info::setMoyenne(double moy)
{
	moyenne = moy;
}

double Info::getEcartType()
{
	return ecartType;
}

void Info::setEcartType(double ec)
{
	ecartType = ec;
}

vector <string> & Info::getValeursS()
{
	return valeursS;
}

map<string, double> & Info::getFrequenceString()
{
	return frequenceString;
}

void Info::setFrequenceString(map<string, double> fs)
{
	frequenceString = fs;
}
